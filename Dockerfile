# Use the official Rust image as the build environment
FROM rust:latest as builder

# Set the working directory
WORKDIR /app

# Copy the source code into the container
ADD . ./

# Get and unwrap dependencies for torch
RUN mkdir /ld_lib
RUN apt-get update -y && \
  apt-get install -y pkg-config make g++ libssl-dev
COPY ./ld_lib /ld_lib

ENV LIBTORCH=/ld_lib/libtorch
ENV LIBTORCH_INCLUDE=/ld_lib/libtorch
ENV LIBTORCH_LIB=/ld_lib/libtorch
ENV LD_LIBRARY_PATH=/ld_lib/libtorch/lib:$LD_LIBRARY_PATH

# Build the application
RUN cargo clean && cargo build --release

# Create a new lightweight container for the application
FROM debian:buster-slim

# Set the working directory
WORKDIR /app

# Since the torch deps are shared libs, they are not packaged in the binary
RUN mkdir /ld_lib
RUN apt-get update -y && \
  apt-get install -y pkg-config make g++ libssl-dev
COPY ./ld_lib /ld_lib

ENV LIBTORCH=/ld_lib/libtorch
ENV LIBTORCH_INCLUDE=/ld_lib/libtorch
ENV LIBTORCH_LIB=/ld_lib/libtorch
ENV LD_LIBRARY_PATH=/ld_lib/libtorch/lib:$LD_LIBRARY_PATH

# Copy the compiled binary from the builder stage into the lightweight container
COPY --from=builder /app/target/release/ns380-transformer-lambda .

# Command to run the application
CMD ["./ns380-transformer-lambda"]
