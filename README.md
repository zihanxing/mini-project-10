# Week 10 Mini-Project
## Developed by
Zach Xing

This project involves creating a lambda function that uses the Rust-BERT crate to deliver answers to questions submitted through a lambda service.

## Construction Steps

The setup process for this lambda function is quite intricate. Initially, acquire the LibTorch distribution by downloading it from [this link](https://pytorch.org/get-started/locally/). My specific commands were:

```
wget https://download.pytorch.org/libtorch/cpu/libtorch-cxx11-abi-shared-with-deps-2.1.0%2Bcpu.zip
unzip libtorch-cxx11-abi-shared-with-deps-2.1.0+cpu.zip -d ./ld_lib/
```

It's critical to use version 2.1.0 as it ensures compatibility with the rust-bert crate available on crates.io. After downloading, you must configure several environment variables.

```
ENV LIBTORCH=./ld_lib/libtorch
ENV LIBTORCH_INCLUDE=./ld_lib/libtorch
ENV LIBTORCH_LIB=./ld_lib/libtorch
ENV LD_LIBRARY_PATH=./ld_lib/libtorch/lib:$LD_LIBRARY_PATH
```

Following these settings, you can proceed to compile the binary using `cargo build`.

For convenience, the binary has been containerized using a Dockerfile which performs the steps necessary to compile it. After compiling, the Docker image is uploaded to ECR with these commands:

```
docker build -t qa .
docker tag <image hash> <ECR Repo URL>:latest
docker push <ECR Repo URL>:latest
```

The lambda is then configured to run using the Docker image from ECR.

## Operational Guidance

This lambda function is designed for interaction via cURL. The correct command to use would be:

```
curl -H "Content-Type: application/json" --data '{"query": "What is IDS", "context": "IDS stands for Interdisciplinary Data Science"}' https://fdesdaw6wl.execute-api.us-west-2.amazonaws.com/defaultl/lambda-transformer
```

Expect the lambda to return a response similar to what BERT might generate.